<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/tarea.php';
require_once '../util/Funciones.php';


try {
    $obj = new tarea();
    $resultado = $obj->listar();

    $listacity = array();
    for ($i = 0; $i < count($resultado); $i++)
    {

        $datos = array(
            "id_tarea" => $resultado[$i]["id_tarea"],
            "titulo" => $resultado[$i]["titulo"],
            "descripcion" => $resultado[$i]["descripcion"],
            "fecha_inicio" => $resultado[$i]["fecha_inicio"],
            "fecha_fin" => $resultado[$i]["fecha_fin"],
            "id_cursos" => $resultado[$i]["id_cursos"],
            "archivo_tarea" => $resultado[$i]["archivo_tarea"],
            "nombre_cursos" => $resultado[$i]["nombre_cursos"]
        );

        $listacity[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacity);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
