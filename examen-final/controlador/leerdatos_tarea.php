<?php

    require_once '../negocio/tarea.php';
    require_once '../util/Funciones.php';

    try {
	$obj = new tarea();
        $id_tarea = $_POST["id_tarea"];
        $obj->setId_tarea($id_tarea);
        
        $resultado = $obj->leerDatos();
	Funciones::imprimeJSON(200, "", $resultado);
	
    } catch (Exception $exc) {
	Funciones::imprimeJSON(500, $exc->getMessage(), "");
	
    }
