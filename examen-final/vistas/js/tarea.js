$(document).ready(function () {
    cargarComboCursos("cbocursos")
    listar()
});

$("#btnAgregar").click(function () {
    $("#modal_id_tarea").val("")
    $("#modal_nombre").val("")
    $("#modal_descripcion").val("")
    $("#modal_fecha_inicio").val("")
    $("#modal_fecha_final").val("")
    cargarComboCursos(cbocursos, "seleccione")
})

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    var id_tarea = $("#modal_id_tarea").val()
    var titulo = $("#modal_nombre").val()
    var descripcion = $("#modal_descripcion").val()
    var fecha_inicio = $("#modal_fecha_inicio").val()
    var fecha_fin = $("#modal_fecha_final").val()
    var archivo = $("#archivo").prop("files")[0]
    var id_cursos = $("#cbocursos").val()

    var form_data = new FormData();
    form_data.append("titulo", titulo);
    form_data.append("descripcion", descripcion);
    form_data.append("fecha_inicio", fecha_inicio);
    form_data.append("fecha_fin", fecha_fin);
    form_data.append("archivo", archivo);
    form_data.append("id_cursos", id_cursos);


    if (id_tarea != "") {
        
        form_data.append("id_tarea", id_tarea);

        $.ajax({
            url: "../controlador/editar_tarea_cursos.php",
            type: "POST",
            data: form_data,
            enctype: 'multipart/form-data',
            processData: false, // tell jQuery not to process the data
            contentType: false,
            success: function (data) {
                alert("se agregado correctamente")
                $("#signup-modal").modal("hide")
                listar()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown)
            }
        })
    } else {
        $.ajax({
            url: "../controlador/agregar_tarea_cursos.php",
            type: "POST",
            data: form_data,
            enctype: 'multipart/form-data',
            processData: false, // tell jQuery not to process the data
            contentType: false,
            success: function (data) {
                alert("se agregado correctamente")
                $("#signup-modal").modal("hide")
                listar()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown)
            }
        })
    }
}
);

function listar() {


    $.post("../controlador/listar_tarea.php", function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado3" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th>Tarea</th>';
            html += '<th>Titulo</th>';
            html += '<th>Descripcion</th>';
            html += '<th>Fecha Inicio</th>';
            html += '<th>Fecha fin</th>';
            html += '<th>Curso</th>';
            html += '<th>Nombre Archivo</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td align="center">' + item.id_tarea + '</td>';
                html += '<td>' + item.titulo + '</td>';
                html += '<td>' + item.descripcion + '</td>';
                html += '<td>' + item.fecha_inicio + '</td>';
                html += '<td>' + item.fecha_fin + '</td>';
                html += '<td>' + item.nombre_cursos + '</td>';
                html += '<td>' + item.archivo_tarea + '</td>';

                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#signup-modal" onclick="leerDatos(' + item.id_tarea + ')"><i class="fa fa-pencil"></i></button>';
                html += '</td>';

                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);
        } else {
            alert("error")
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        alert("error: "+datosJSON)
    })
}

function leerDatos(id_tarea) {

    $.post("../controlador/leerdatos_tarea.php", {id_tarea: id_tarea}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {

            $("#modal_id_tarea").val(datosJSON.datos.id_tarea)
            $("#modal_nombre").val(datosJSON.datos.titulo)
            $("#modal_descripcion").val(datosJSON.datos.descripcion)
            $("#modal_fecha_inicio").val(datosJSON.datos.fecha_inicio)
            $("#modal_fecha_final").val(datosJSON.datos.fecha_fin)
            $("#cbocursos").val(datosJSON.datos.id_cursos)

        } else {
            alert("error")
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        alert("error: "+datosJSON)
    })
}
