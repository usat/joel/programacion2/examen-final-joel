$(document).ready(function () {
    listar()
});

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();
    var nombre = $("#modal_nombre").val()
    $.post("../controlador/agregar_cursos.php", {titulo: nombre}, function () {}).done(function (resultado) {
        if (resultado.estado == 200) {
            alert("se agregado correctamente")
            $("#signup-modal2").modal("hide")
            listar()
        } else {
            alert("no se agregado correctamente")
        }
    })
}
);

function listar() {

    $.post("../controlador/listar_cursos.php", function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado3" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th>Curso</th>';
            html += '<th>Nombre Cursos</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td>' + item.id_cursos + '</td>';
                html += '<td>' + item.nombre_cursos + '</td>';

                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);
        } else {
            alert(resultado)
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        alert("error: "+error)
    })
}