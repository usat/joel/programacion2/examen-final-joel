-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: examen-final
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos` (
  `id_cursos` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_cursos` varchar(100) NOT NULL,
  PRIMARY KEY (`id_cursos`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos`
--

LOCK TABLES `cursos` WRITE;
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` VALUES (1,'asdasdasd'),(2,'vasdrfwerwe'),(3,'asdasdq12312'),(4,'111'),(5,'21dasd'),(6,'asdlasdka123'),(7,'silviopd');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarea`
--

DROP TABLE IF EXISTS `tarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarea` (
  `id_tarea` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `archivo_tarea` varchar(100) DEFAULT NULL,
  `id_cursos` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_tarea`),
  KEY `idx_tarea_id_cursos` (`id_cursos`),
  CONSTRAINT `fk_tarea_cursos` FOREIGN KEY (`id_cursos`) REFERENCES `cursos` (`id_cursos`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarea`
--

LOCK TABLES `tarea` WRITE;
/*!40000 ALTER TABLE `tarea` DISABLE KEYS */;
INSERT INTO `tarea` VALUES (1,'2silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(2,'silviopd1235555','kjhhjh','2018-01-02','2019-02-01',NULL,7),(3,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(4,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(5,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(6,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(7,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(8,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(9,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(10,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(11,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(12,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(13,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(14,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(15,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(16,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(17,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(18,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(19,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(20,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(21,'silviopd123','kjhhjh','2018-01-02','2019-02-01','subir.jpg',1),(22,'dasd','asdasd','2018-11-03','2018-11-03','subir.jpg',1);
/*!40000 ALTER TABLE `tarea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'examen-final'
--

--
-- Dumping routines for database 'examen-final'
--
/*!50003 DROP PROCEDURE IF EXISTS `editar_tarea_cursos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `editar_tarea_cursos`(IN v_id_tarea INT,IN titulo varchar(100), IN descripcion varchar(100),IN fecha_inicio date,IN fecha_fin date,IN archivo_tarea varchar(100),IN id_curso int)
    NO SQL
BEGIN

	UPDATE `tarea`
	SET
	`titulo` = titulo,
	`descripcion` = descripcion,
	`fecha_inicio` = fecha_inicio,
	`fecha_fin` = fecha_fin,
	`archivo_tarea` = archivo_tarea,
	`id_cursos` = id_curso
	WHERE `id_tarea` = v_id_tarea;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registrar_cursos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `registrar_cursos`(IN titulo varchar(100))
    NO SQL
BEGIN

INSERT INTO cursos
	( nombre_cursos) VALUES ( titulo );

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registrar_tarea_cursos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `registrar_tarea_cursos`(IN titulo varchar(100), IN descripcion varchar(100),IN fecha_inicio date,IN fecha_fin date,IN archivo_tarea varchar(100),IN id_curso int)
    NO SQL
BEGIN

DECLARE v_id_tarea int;
    
  select count(*)+1 into v_id_tarea from tarea;  
  
  INSERT INTO tarea
	( id_tarea, titulo, descripcion, fecha_inicio, fecha_fin, archivo_tarea, id_cursos) 
    VALUES ( v_id_tarea, titulo, descripcion, fecha_inicio, fecha_fin, archivo_tarea, id_curso );

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-27 20:29:38
